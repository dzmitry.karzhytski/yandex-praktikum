# Yandex Praktikum

## Тестовое задание на вакансию наставника в Яндекс Практикум.

### https://forms.yandex.ru/u/5e746034dfd2660a3739b123/

## Тестовая модель:
#### Вариант 1:
#### https://docs.google.com/document/d/1QwiO8RXsQvrbfHeCxIkPpxh8jNeGg5Ln0OVXFCtd8D0/edit?usp=sharing 
#### Вариант 2:
#### https://drive.google.com/file/d/1EZ4MdyLBhnYyqCw2fRACqH-n_hS7Zb9q/view?usp=sharing


### Getting started

1.Git

```bash
https://gitlab.com/dzmitry.karzhytski/yandex-praktikum.git
```
2.Изменить входные данные

```bash
$ nano reference.conf
edit reference.conf
```
3.Запустить все шаги (очистить, настроить окружение и запустить тесты)
```bash
$ make all
running all make steps
```

### Project structure

```Structure
.
├── tests/.   (В этой папке все тесты)
          └── test_e2e.py (Вариант №2 для Задачи №3 (автотесты))  
├── credit_calculator.py (Главная функция)
├── Makefile (Пользователи могут взаимодействовать с тестами через make)
├── README.md
├── reference.conf (В этом файле задаем входные данные)
├── settings.py
└── requirements.txt (All required python packages should be stored in this file)