from math import log

from settings import (
    REQUESTED_AMOUNT,
    MATURITY,
    REVENUE,
    SOURCE_OF_INCOME,
    PURPOSE,
    CREDIT_RATING,
    SEX,
    AGE,
    RETIREMENT_AGE_MAN,
    RETIREMENT_AGE_FEMALE,
    BASE_RATE,
)

SUM_SOURCE = {
    "пассивный доход": 1,
    "наёмный работник": 5,
    "собственный бизнес": 10,
}
SUM_RATING = {-1: 1, 0: 5, 1: 10, 2: 10}
RATE_PURPOSE = {
    "ипотека": -2,
    "развитие бизнеса": -0.5,
    "потребительский": 1.5,
    "автокредит": 0,
}
RATE_SOURCE = {
    "пассивный доход": 0.5,
    "наёмный работник": -0.25,
    "собственный бизнес": 0.25,
}
RATE_RATING = {-1: 1.5, 0: 0, 1: -0.25, 2: -0.75}


def check_adult(age):
    if age < 18:
        return 0
    else:
        return 1


def condition_one(sex, age, maturity):
    if sex == "M":
        if age + maturity > RETIREMENT_AGE_MAN:
            return 0
        else:
            return 1
    elif sex == "F":
        if age + maturity > RETIREMENT_AGE_FEMALE:
            return 0
        else:
            return 1


def condition_two(request, maturity, revenue):
    if request / maturity > revenue / 3:
        return 0
    else:
        return 1


def condition_three(rating):
    if rating == -2:
        return 0
    else:
        return 1


def condition_four(source):
    if source == "безработный":
        return 0
    else:
        return 1


def condition_five(payment, revenue):
    if payment > revenue / 2:
        return 0
    else:
        return 1


def condition_source_sum(source, amount):
    if source in SUM_SOURCE:
        if amount > SUM_SOURCE[source]:
            return 0
        else:
            return 1
    else:
        return 0


def condition_rating_sum(rating, amount):
    if rating in SUM_RATING:
        if amount > SUM_RATING[rating]:
            return 0
        else:
            return 1
    else:
        return 0


def change_rate_purpose(purpose):
    if purpose in RATE_PURPOSE:
        return RATE_PURPOSE[purpose]
    else:
        return 0


def change_rate_source(source):
    if source in RATE_SOURCE:
        return RATE_SOURCE[source]
    else:
        return 0


def change_rate_rating(rating):
    if rating in RATE_RATING:
        return RATE_RATING[rating]
    else:
        return 0


def change_rate_request(request):
    return -log(request, 10)


def annual_payment(request, maturity, source, rating, purpose):
    return (
        request
        * (
            1
            + maturity
            * (
                BASE_RATE
                + change_rate_purpose(purpose)
                + change_rate_source(source)
                + change_rate_rating(rating)
                + change_rate_request(request)
            )
            / 100
        )
        / maturity
    )


def result(request, maturity, revenue, source, rating, sex, age, purpose):
    if (
        check_adult(age)
        + condition_one(sex, age, maturity)
        + condition_two(request, maturity, revenue)
        + condition_three(rating)
        + condition_four(source)
        + condition_source_sum(source, request)
        + condition_rating_sum(rating, request)
    ) < 7:
        return "КРЕДИТ НЕ ВЫДАЕТСЯ."

    else:
        payment = annual_payment(request, maturity, source, rating, purpose)
        if condition_five(payment, revenue):
            return """1. КРЕДИТ ВЫДАЕТСЯ.
2. ГОДОВОЙ ПЛАТЕЖ ПО КРЕДИТУ: ~{:.3f} млн.""".format(
                payment
            )
        else:
            return "КРЕДИТ НЕ ВЫДАЕТСЯ."


if __name__ == "__main__":
    print(
        result(
            REQUESTED_AMOUNT,
            MATURITY,
            REVENUE,
            SOURCE_OF_INCOME,
            CREDIT_RATING,
            SEX,
            AGE,
            PURPOSE,
        )
    )
