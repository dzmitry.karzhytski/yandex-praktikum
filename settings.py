from os import environ
from pyhocon import ConfigFactory

conf = ConfigFactory.parse_file("reference.conf")
AGE = environ.get("AGE", conf.get_int("Age"))
SEX = environ.get("SEX", conf.get_string("Sex"))
SOURCE_OF_INCOME = environ.get(
    "SOURCE_OF_INCOME", conf.get_string("Source_of_income")
)
REVENUE = environ.get("REVENUE", conf.get_int("Revenue_last_year"))
CREDIT_RATING = environ.get("CREDIT_RATING", conf.get_int("Credit_rating"))
REQUESTED_AMOUNT = environ.get(
    "REQUESTED_AMOUNT", conf.get_float("Requested_amount")
)
MATURITY = environ.get("MATURITY", conf.get_int("Maturity"))
PURPOSE = environ.get("PURPOSE", conf.get_string("Purpose"))
RETIREMENT_AGE_MAN = environ.get(
    "Retirement_age_Man", conf.get_int("Retirement_age_Man")
)
RETIREMENT_AGE_FEMALE = environ.get(
    "Retirement_age_Female", conf.get_int("Retirement_age_Female")
)

BASE_RATE = 10
CHECK_OK = 1
CHECK_FAILED = 0
