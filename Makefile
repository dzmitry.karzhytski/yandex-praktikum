.PHONY: help venv test

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  clean       => to clean clean all automatically generated files"
	@echo "  venv        => to create a virtualenv"
	@echo "  test        => to run tests"
	@echo "  all         => clean venv test"

all: clean venv test

clean:
	@find . -name \*.pyc -delete
	@find . -name \*__pycache__ -delete
	@find . -name \.pytest_cache -exec rm -r {} +
	@rm -f junit.xml || true
	@rm -rf ./dist || true
	@rm -rf venv || true


venv:
	@python3 -m venv venv
	@venv/bin/pip install -U -r requirements.txt

test:
	@venv/bin/python3 -m pytest tests
