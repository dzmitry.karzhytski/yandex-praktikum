from hamcrest import assert_that, equal_to
from pytest import fixture

from credit_calculator import condition_source_sum, condition_rating_sum
from settings import CHECK_OK, CHECK_FAILED


@fixture(
    params=[
        ("пассивный доход", 1),
        ("пассивный доход", 0.9),
        ("пассивный доход", 0.1),
        ("наёмный работник", 5),
        ("наёмный работник", 4.99),
        ("наёмный работник", 2),
        ("собственный бизнес", 10),
    ]
)
def source_ok(request):
    return request.param


@fixture(
    params=[
        ("пассивный доход", 1.1),
        ("пассивный доход", 9),
        ("наёмный работник", 5.1),
        ("наёмный работник", 8.99),
    ]
)
def source_failed(request):
    return request.param


@fixture(
    params=[
        (-1, 1),
        (-1, 0.9),
        (-1, 0.1),
        (0, 5),
        (0, 4.99),
        (0, 2),
        (1, 10),
        (2, 10),
    ]
)
def rating_ok(request):
    return request.param


@fixture(
    params=[(-1, 1.1), (-1, 9), (0, 5.1), (0, 8.99),]
)
def rating_failed(request):
    return request.param


def test_condition_source_ok(source_ok):
    source, amount = source_ok
    assert_that(condition_source_sum(source, amount), equal_to(CHECK_OK))


def test_condition_source_failed(source_failed):
    source, amount = source_failed
    assert_that(condition_source_sum(source, amount), equal_to(CHECK_FAILED))


def test_condition_rating_ok(rating_ok):
    rating, amount = rating_ok
    assert_that(condition_rating_sum(rating, amount), equal_to(CHECK_OK))


def test_condition_rating_failed(rating_failed):
    rating, amount = rating_failed
    assert_that(condition_rating_sum(rating, amount), equal_to(CHECK_FAILED))
