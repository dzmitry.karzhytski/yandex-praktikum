from hamcrest import assert_that, equal_to
from pytest import fixture

from credit_calculator import annual_payment, condition_five
from settings import CHECK_OK, CHECK_FAILED


@fixture(
    params=[
        (10, 10, "наёмный работник", 2, "ипотека", 1.6),
        (0.1, 1, "пассивный доход", 0, "автокредит", 0.1115),
        (10, 20, "собственный бизнес", 1, "развитие бизнеса", 1.35),
    ]
)
def annual_payment_params(request):
    return request.param


@fixture(
    params=[
        (15, 10, 10, "наёмный работник", 2, "ипотека", CHECK_OK),
        (3.2, 10, 10, "наёмный работник", 2, "ипотека", CHECK_OK),
        (3.1, 10, 10, "наёмный работник", 2, "ипотека", CHECK_FAILED),
        (0.4, 10, 10, "наёмный работник", 2, "ипотека", CHECK_FAILED),
    ]
)
def payment_revenue_params(request):
    return request.param


def test_annual_payment(annual_payment_params):
    (
        request,
        maturity,
        source,
        rating,
        purpose,
        expected_result,
    ) = annual_payment_params
    assert_that(
        annual_payment(request, maturity, source, rating, purpose),
        equal_to(expected_result),
    )


def test_condition_five(payment_revenue_params):
    (
        revenue,
        request,
        maturity,
        source,
        rating,
        purpose,
        expected_result,
    ) = payment_revenue_params
    payment = annual_payment(request, maturity, source, rating, purpose)
    assert_that(condition_five(payment, revenue), equal_to(expected_result))
