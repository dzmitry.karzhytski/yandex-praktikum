from hamcrest import assert_that, equal_to
from pytest import fixture

from credit_calculator import (
    check_adult,
    condition_one,
    condition_two,
    condition_three,
    condition_four,
)
from settings import CHECK_OK, CHECK_FAILED


@fixture(params=[25, 18, 19])
def age_ok(request):
    return request.param


@fixture(params=[17, 3])
def age_failed(request):
    return request.param


@fixture(params=[("M", 45, 17), ("M", 52, 13), ("F", 28, 7), ("F", 59, 1)])
def age_conditions_ok(request):
    return request.param


@fixture(params=[("M", 65, 1), ("M", 75, 20), ("F", 58, 3), ("F", 100, 1)])
def age_conditions_failed(request):
    return request.param


@fixture(params=[(5, 20, 15), (10, 10, 3), (6, 2, 9)])
def money_ok(request):
    return request.param


@fixture(params=[(10, 3, 5), (0.1, 1, 0.2)])
def money_failed(request):
    return request.param


@fixture(params=[-1, 0, 2])
def rating_ok(request):
    return request.param


@fixture(params=[-2])
def rating_failed(request):
    return request.param


@fixture(params=["пассивный доход", "наёмный работник", "собственный бизнес"])
def source_ok(request):
    return request.param


@fixture(params=["безработный"])
def source_failed(request):
    return request.param


def test_adult_client_ok(age_ok):
    assert_that(check_adult(age_ok), equal_to(CHECK_OK))


def test_adult_client_failed(age_failed):
    assert_that(check_adult(age_failed), equal_to(CHECK_FAILED))


def test_condition_one_ok(age_conditions_ok):
    sex, age, maturity = age_conditions_ok
    assert_that(condition_one(sex, age, maturity), equal_to(CHECK_OK))


def test_condition_one_failed(age_conditions_failed):
    sex, age, maturity = age_conditions_failed
    assert_that(condition_one(sex, age, maturity), equal_to(CHECK_FAILED))


def test_condition_two_ok(money_ok):
    request, maturity, revenue = money_ok
    assert_that(condition_two(request, maturity, revenue), equal_to(CHECK_OK))


def test_condition_two_failed(money_failed):
    request, maturity, revenue = money_failed
    assert_that(
        condition_two(request, maturity, revenue), equal_to(CHECK_FAILED)
    )


def test_condition_three_ok(rating_ok):
    assert_that(condition_three(rating_ok), equal_to(CHECK_OK))


def test_condition_three_failed(rating_failed):
    assert_that(condition_three(rating_failed), equal_to(CHECK_FAILED))


def test_condition_four_ok(source_ok):
    assert_that(condition_four(source_ok), equal_to(CHECK_OK))


def test_condition_four_failed(source_failed):
    assert_that(condition_four(source_failed), equal_to(CHECK_FAILED))
