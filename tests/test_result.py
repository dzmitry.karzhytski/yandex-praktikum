from hamcrest import assert_that, starts_with
from pytest import fixture

from credit_calculator import result


@fixture(
    params=[
        (
            0.1,
            20,
            150,
            "наёмный работник",
            2,
            "M",
            25,
            "ипотека",
            "1. КРЕДИТ ВЫДАЕТСЯ.",
        ),
        (
            10,
            1,
            4,
            "безработный",
            -2,
            "F",
            65,
            "автокредит",
            "КРЕДИТ НЕ ВЫДАЕТСЯ.",
        ),
    ]
)
def result_params(request):
    return request.param


def test_result(result_params):
    (
        request,
        maturity,
        revenue,
        source,
        rating,
        sex,
        age,
        purpose,
        expected_result,
    ) = result_params
    assert_that(
        result(request, maturity, revenue, source, rating, sex, age, purpose),
        starts_with(expected_result),
    )
