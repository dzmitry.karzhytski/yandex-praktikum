from hamcrest import assert_that, equal_to
from pytest import fixture

from credit_calculator import (
    change_rate_purpose,
    change_rate_source,
    change_rate_rating,
    change_rate_request,
)


@fixture(
    params=[
        ("ипотека", -2),
        ("развитие бизнеса", -0.5),
        ("автокредит", 0),
        ("потребительский", 1.5),
    ]
)
def purpose_rate(request):
    return request.param


@fixture(
    params=[
        ("пассивный доход", 0.5),
        ("наёмный работник", -0.25),
        ("собственный бизнес", 0.25),
    ]
)
def source_rate(request):
    return request.param


@fixture(
    params=[(-1, 1.5), (0, 0), (1, -0.25), (2, -0.75),]
)
def rating_rate(request):
    return request.param


@fixture(
    params=[(0.1, 1), (1, 0), (10, -1),]
)
def request_rate(request):
    return request.param


def test_change_rate_purpose(purpose_rate):
    purpose, expected_result = purpose_rate
    assert_that(change_rate_purpose(purpose), equal_to(expected_result))


def test_change_rate_source(source_rate):
    source, expected_result = source_rate
    assert_that(change_rate_source(source), equal_to(expected_result))


def test_change_rate_rating(rating_rate):
    rating, expected_result = rating_rate
    assert_that(change_rate_rating(rating), equal_to(expected_result))


def test_change_rate_request(request_rate):
    request, expected_result = request_rate
    assert_that(round(change_rate_request(request)), equal_to(expected_result))
